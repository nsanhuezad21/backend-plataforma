import os

import random
import string

import hashlib 

from passlib.hash import pbkdf2_sha256



def generator(original_route):

	separator = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(10)])

	#sec = pbkdf2_sha256.using(salt_size=8).hash(original_route)

	sec = hashlib.sha256(original_route.encode())

	#un token random que es irrelevante en la ruta

	numero_random = random.randint(10,100)
	pre_token = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(numero_random)])

	#borrar el tipo de encriptacion en el comienzo del string

	#sha = sec.replace('$pbkdf2-sha256$29000$','')

	#obviamente esto debe ser un render_template luego

	#link = pre_token+separator+str(sha)

	link = str(sec.hexdigest())

	return link.replace('/','')



