from werkzeug.serving import run_simple

from plataforma import get_app

to_reload = False

class AppReloader(object):
	def __init__(self, create_app):
		self.create_app = create_app
		self.app = create_app()

	def get_application(self):

		global to_reload
		if to_reload:
			self.app = self.create_app()
			to_reload = False

		return self.app

	def __call__(self, environ, start_response):

		app = self.get_application()
		return app(environ, start_response)


# This application object can be used in any WSGI server
# for example in gunicorn, you can run "gunicorn app"


if __name__ == '__main__':

	application = AppReloader(get_app)
	run_simple('0.0.0.0', 8000, application, use_reloader=True, use_debugger=True, use_evalex=True)

application = get_app()
