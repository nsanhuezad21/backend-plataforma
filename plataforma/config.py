from plataforma.engine import engine_connection_string

class Config(object):
	DEBUG = False
	TESTING = False
	#DATABASE = 'sqlite://:memory:'

class ProductionConfig(Config):
	BASE_URL = "http://0.0.0.0:5000"
	SQLALCHEMY_DATABASE_URI = engine_connection_string
	SQLALCHEMY_BINDS = {
		'plt': SQLALCHEMY_DATABASE_URI,
	}

	SQLALCHEMY_TRACK_MODIFICATIONS = False
	#Muestra por consola todas las peticiones Sql
	SQLALCHEMY_ECHO = False
	SEND_FILE_MAX_AGE_DEFAULT = 0
	# True = muesta errores en navegador False= muestra errores en consola
	PROPAGATE_EXCEPTIONS = False

class DevelopmentConfig(Config):

	BASE_URL = "http://0.0.0.0:5000"
	SQLALCHEMY_DATABASE_URI = engine_connection_string
	SQLALCHEMY_BINDS = {
		'plt': SQLALCHEMY_DATABASE_URI,
	}

	SQLALCHEMY_TRACK_MODIFICATIONS = True
	#Muestra por consola todas las peticiones Sql
	SQLALCHEMY_ECHO = True
	SEND_FILE_MAX_AGE_DEFAULT = 1
	# True = muesta errores en navegador False= muestra errores en consola
	PROPAGATE_EXCEPTIONS = True

class TestingConfig(Config):
	TESTING = True
	PORT = 8000
	HOST = '0.0.0.0'


app_config = {
	'development': DevelopmentConfig,
	'testing': TestingConfig,
	'production': ProductionConfig }
