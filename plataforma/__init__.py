# -*- codig utf-8 -*-
from plataforma.use_cases.menu_use_case import MenuUseCase
from plataforma.use_cases.parametro_use_case import ParametroUseCase
from plataforma.use_cases.perfil_use_case import PerfilUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase
from plataforma.use_cases.sesiones_use_case import SesionUseCase
from plataforma.use_cases.usuario_use_case import UsuarioUseCase
from plataforma.use_cases.password_use_case import PasswordUseCase
from plataforma.use_cases.login_use_case import LoginUseCase
from plataforma.use_cases.active_use_case import ActiveUseCase
from plataforma.use_cases.cliente_use_case import ClienteUseCase

from plataforma.use_cases.configure_use_case_binding import configure_use_case_binding

from plataforma.views.class_login_views import Login_View
from plataforma.views.class_menus_views import Menus_View
from plataforma.views.class_parametro_views import Parametro_View
from plataforma.views.class_perfil_views import Perfil_View
from plataforma.views.class_permiso_views import Permiso_View
from plataforma.views.class_usuario_views import Usuario_View
from plataforma.views.class_active_views import Active_View

from plataforma.repositories.sql_alchemy.menus_sql_alchemy import MenuRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.parametros_sql_alchemy import ParametrosRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.perfiles_sql_alchemy import PerfilRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.permiso_sql_alchemy import permisoRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.sesiones_sql_alchemy import SesionRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.usuario_sql_alchemy import UsuarioRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.password_sql_alchemy import PasswordRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.login_sql_alchemy import LoginRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.cliente_sql_alchemy import ClienteRepositorySqlAlchemy


from plataforma.repositories.sql_alchemy.mapping.menus_mapping import menu_mapping
from plataforma.repositories.sql_alchemy.mapping.parametros_mapping import parametro_mapping
from plataforma.repositories.sql_alchemy.mapping.perfiles_mapping import perfil_mapping
from plataforma.repositories.sql_alchemy.mapping.permisos_mapping import permiso_mapping
from plataforma.repositories.sql_alchemy.mapping.sesiones_mapping import session_mapping
from plataforma.repositories.sql_alchemy.mapping.usuario_mapping import usuario_mapping
from plataforma.repositories.sql_alchemy.mapping.cliente_mapping import cliente_mapping
from plataforma.repositories.sql_alchemy.mapping.password_mapping import password_mapping
from plataforma.repositories.sql_alchemy.mapping.login_mapping import login_mapping


from flask_restful import Resource, Api
from flask_cors import CORS
# from flask_sslify import SSLify

from sqlalchemy import MetaData
from injector import Binder, singleton
from flask_injector import FlaskInjector
from sqlalchemy.orm import clear_mappers
from flask_sqlalchemy import SQLAlchemy
import os

from flask import (Flask, Blueprint, g, render_template, flash,
                   url_for, redirect, abort, request, session, send_from_directory)

from werkzeug import secure_filename
from werkzeug.exceptions import HTTPException


# config
from plataforma.config import app_config

# subclase de flask para ofuscar las rutas en el registro sobreescribe app.route y add_url_rule
from flask_instance import FlaskInstance

# middleware puede intersectar request y response, para ofuscar querystring
import middleware

def get_app():
    app = FlaskInstance(__name__)
    app.debug = app.config["ENV"] == "development"

    #development / test / production
    config_obj = app_config[app.config["ENV"]]

    app.jinja_env.add_extension('jinja2.ext.loopcontrols')

    # config que puede variar de development, testing o produccion
    app.config.from_object(config_obj)

    # dependencia del framework de seguridad
    # app.wsgi_app = middleware.SimpleMiddleWare(app.wsgi_app)



    # framework seguridad para parametros, hacerlo disponible en templates
    # app.jinja_env.globals.update(qs=app.wsgi_app.generate_qs)

    modules_list = [configure_database, configure_use_case_binding]

    FlaskInjector(app=app, modules=modules_list)

    # --REPOSITORIO--

    # a falta de injector se instancia db para los repos
    api = Api(app)
    # sslify = SSLify(app)
    CORS(app)

    db = SQLAlchemy(app)

    # instanciar repositorios sqlalchemy
    menu_repo = MenuRepositorySqlAlchemy(db)
    parametro_repo = ParametrosRepositorySqlAlchemy(db)
    perfil_repo = PerfilRepositorySqlAlchemy(db)
    permiso_repo = permisoRepositorySqlAlchemy(db)
    sesion_repo = SesionRepositorySqlAlchemy(db)
    usuario_repo = UsuarioRepositorySqlAlchemy(db)
    password_repo = PasswordRepositorySqlAlchemy(db)
    login_repo = LoginRepositorySqlAlchemy(db)
    cliente_repo = ClienteRepositorySqlAlchemy(db)

    # instanciar casos de usos
    muc = MenuUseCase(menu_repo)
    ptuc = ParametroUseCase(parametro_repo)
    pfuc = PerfilUseCase(perfil_repo)
    pruc = PermisoUseCase(permiso_repo)
    suc = SesionUseCase(sesion_repo)
    uuc = UsuarioUseCase(usuario_repo)
    pauc = PasswordUseCase(password_repo)
    luc = LoginUseCase(login_repo)
    auc = ActiveUseCase(menu_repo, perfil_repo, permiso_repo, usuario_repo)
    cluc = ClienteUseCase(cliente_repo)


    # vistas
    login_view = Login_View(uuc, pfuc, pruc, pauc, suc, luc, ptuc)
    usuario_view = Usuario_View(uuc, pfuc, pruc, suc, cluc, pauc)
    permiso_view = Permiso_View(uuc, pfuc, pruc, suc, muc)
    menus_view = Menus_View(muc, uuc, pfuc, pruc, suc)
    active_view = Active_View(auc, suc, pruc)
    perfil_view = Perfil_View(uuc, pfuc, pruc, suc)

    # generar rutas
    generate_login_routes(app, login_view)
    generate_usuarios_routes(app, usuario_view)
    generate_permiso_routes(app, permiso_view)
    generate_menus_routes(app, menus_view)
    generate_active_routes(app, active_view)
    generate_perfil_routes(app, perfil_view)

    # @app.errorhandler(500)
    # def page_not_found(e):
    #     # note that we set the 404 status explicitly
    #     return render_template('error500.html'), 500

    return app


def configure_database(binder: Binder) -> Binder:

    application = binder.injector.get(Flask)
    metadata = MetaData()

    if True:
        cliente_mapping(metadata)
        menu_mapping(metadata)
        perfil_mapping(metadata)
        parametro_mapping(metadata)
        permiso_mapping(metadata)
        usuario_mapping(metadata)
        session_mapping(metadata)
        password_mapping(metadata)
        login_mapping(metadata)

    db = SQLAlchemy(application)
    metadata.create_all(db.engine)
    db.session.commit()
    return binder

def generate_active_routes(app, active_view):

    app.view_functions['active'] = active_view.change_Active

    app.add_url_rule('active/change_Active', 'plt_adelaida.change_Active', active_view.change_Active, methods=['GET', 'POST'])

def generate_login_routes(app, login_view):
    app.view_functions['login'] = login_view.login
    app.view_functions['ingreso'] = login_view.ingreso
    app.view_functions['menu_principal'] = login_view.menu_principal
    app.view_functions['registro'] = login_view.registro
    app.view_functions['registrar'] = login_view.registrar
    app.view_functions['logout'] = login_view.logout

    app.add_url_rule('/', 'plt_adelaida.login', login_view.login, methods=['GET', 'POST'])
    app.add_url_rule('/ingreso', 'plt_adelaida.ingreso', login_view.ingreso, methods=['GET', 'POST'])
    app.add_url_rule('/menu_principal', 'plt_adelaida.menu_principal', login_view.menu_principal, methods=['GET', 'POST'])
    app.add_url_rule('/registro', 'plt_adelaida.registro', login_view.registro, methods=['GET', 'POST'])
    app.add_url_rule('/registrar', 'plt_adelaida.registrar', login_view.registrar, methods=['GET', 'POST'])
    app.add_url_rule('/logout', 'plt_adelaida.logout', login_view.logout, methods=['GET', 'POST'])

def generate_usuarios_routes(app, usuario_view):

    app.view_functions['usuario'] = usuario_view.usuario
    app.view_functions['listar_usuario'] = usuario_view.listar_usuario
    app.view_functions['validar_username'] = usuario_view.validar_username
    app.view_functions['validar_email'] = usuario_view.validar_email
    app.view_functions['add_user'] = usuario_view.add_user
    app.view_functions['edit_user'] = usuario_view.edit_user
    app.view_functions['delete_user'] = usuario_view.delete_user

    app.add_url_rule('/usuario/usuario', 'plt_adelaida.usuario', usuario_view.usuario, methods=['GET', 'POST'])
    app.add_url_rule('/usuario/listar_usuario', 'plt_adelaida.listar_usuario', usuario_view.listar_usuario, methods=['GET', 'POST'])
    app.add_url_rule('/usuario/validar_username', 'plt_adelaida.validar_username', usuario_view.validar_username, methods=['GET', 'POST'])
    app.add_url_rule('/usuario/validar_email', 'plt_adelaida.validar_email', usuario_view.validar_email, methods=['GET', 'POST'])
    app.add_url_rule('/usuario/add_user', 'plt_adelaida.add_user', usuario_view.add_user, methods=['GET', 'POST'])
    app.add_url_rule('/usuario/edit_user', 'plt_adelaida.edit_user', usuario_view.edit_user, methods=['GET', 'POST'])
    app.add_url_rule('/usuario/delete_user', 'plt_adelaida.delete_user', usuario_view.delete_user, methods=['GET', 'POST'])

def generate_permiso_routes(app, permiso_view):

    app.view_functions['Permiso'] = permiso_view.Permiso
    app.view_functions['add_permiso'] = permiso_view.add_permiso
    app.view_functions['listar_permiso'] = permiso_view.listar_permiso
    app.view_functions['cambio_estado'] = permiso_view.cambio_estado
    app.view_functions['delete_permiso'] = permiso_view.delete_permiso

    app.add_url_rule('/Permiso/Permiso', 'plt_adelaida.Permiso', permiso_view.Permiso, methods=['GET', 'POST'])
    app.add_url_rule('/Permiso/add_permiso', 'plt_adelaida.add_permiso', permiso_view.add_permiso, methods=['GET', 'POST'])
    app.add_url_rule('/Permiso/listar_permiso', 'plt_adelaida.listar_permiso', permiso_view.listar_permiso, methods=['GET', 'POST'])
    app.add_url_rule('/Permiso/cambio_estado', 'plt_adelaida.cambio_estado', permiso_view.cambio_estado, methods=['GET', 'POST'])
    app.add_url_rule('/Permiso/delete_permiso', 'plt_adelaida.delete_permiso', permiso_view.delete_permiso, methods=['GET', 'POST'])


def generate_menus_routes(app, menus_view):

    app.view_functions['menus'] = menus_view.menus
    app.view_functions['listar_menus'] = menus_view.listar_menus
    app.view_functions['add_menus'] = menus_view.add_menus
    app.view_functions['cmb_menus'] = menus_view.cmb_menus
    app.view_functions['delete_menu'] = menus_view.delete_menu
    app.view_functions['edit_menu'] = menus_view.edit_menu
    app.view_functions['menu_nav'] = menus_view.menu_nav

    app.add_url_rule('/menus/menus', 'plt_adelaida.menus', menus_view.menus, methods=['GET', 'POST'])
    app.add_url_rule('/menus/listar_menus', 'plt_adelaida.listar_menus', menus_view.listar_menus, methods=['GET', 'POST'])
    app.add_url_rule('/menus/add_menus', 'plt_adelaida.add_menus', menus_view.add_menus, methods=['GET', 'POST'])
    app.add_url_rule('/menus/cmb_menus', 'plt_adelaida.cmb_menus', menus_view.cmb_menus, methods=['GET', 'POST'])
    app.add_url_rule('/menus/delete_menu', 'plt_adelaida.delete_menu', menus_view.delete_menu, methods=['GET', 'POST'])
    app.add_url_rule('/menus/edit_menu', 'plt_adelaida.edit_menu', menus_view.edit_menu, methods=['GET', 'POST'])
    app.add_url_rule('/menus/menu_nav', 'plt_adelaida.menu_nav', menus_view.menu_nav, methods=['GET', 'POST'])

def generate_perfil_routes(app, perfil_view):

    app.view_functions['perfil'] = perfil_view.perfil
    app.view_functions['listar_perfil'] = perfil_view.listar_perfil
    app.view_functions['add_perfil'] = perfil_view.add_perfil
    app.view_functions['delete_perfil'] = perfil_view.delete_perfil

    app.add_url_rule('/perfil/perfil', 'plt_adelaida.perfil', perfil_view.perfil, methods=['GET', 'POST'])
    app.add_url_rule('/perfil/listar_perfil', 'plt_adelaida.listar_perfil', perfil_view.listar_perfil, methods=['GET', 'POST'])
    app.add_url_rule('/perfil/add_perfil', 'plt_adelaida.add_perfil', perfil_view.add_perfil, methods=['GET', 'POST'])
    app.add_url_rule('/perfil/delete_perfil', 'plt_adelaida.delete_perfil', perfil_view.delete_perfil, methods=['GET', 'POST'])