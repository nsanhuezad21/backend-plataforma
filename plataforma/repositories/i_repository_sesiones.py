from abc import ABC, abstractmethod

from plataforma.entities.sesiones import Session

class ISessionRepository(ABC):

	@abstractmethod
	def add(self, session: Session):

		pass

	@abstractmethod
	def get_all(self):

		pass