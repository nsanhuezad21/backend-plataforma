from abc import ABC, abstractmethod

from plataforma.entities.parametros import Parametros

class IParametrosRepository(ABC):

	@abstractmethod
	def add(self, parametro: Parametros):

		pass

	@abstractmethod
	def get_all(self):

		pass