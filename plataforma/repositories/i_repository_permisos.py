from abc import ABC, abstractmethod

from plataforma.entities.permisos import Permiso

class IPermisoRepository(ABC):

	@abstractmethod
	def add(self, permiso: Permiso):

		pass

	@abstractmethod
	def get_all(self):

		pass