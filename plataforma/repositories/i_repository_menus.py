from abc import ABC, abstractmethod

from plataforma.entities.menus import Menus

class IMenuRepository(ABC):

	@abstractmethod
	def add(self, menu: Menus):

		pass

	@abstractmethod
	def get_all(self):

		pass