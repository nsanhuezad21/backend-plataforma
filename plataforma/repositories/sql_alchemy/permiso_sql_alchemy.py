from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError
from sqlalchemy import desc, asc
from sqlalchemy import and_
from sqlalchemy import func, text

from sqlalchemy import cast, Date

from plataforma.entities.permisos import Permiso

from plataforma.repositories.i_repository_permisos import IPermisoRepository

class permisoRepositorySqlAlchemy(IPermisoRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, permiso: Permiso):

		try:
			self.db.session.add(permiso)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()


	def get_all(self):

		return self.db.session.query(Permiso).order_by(Permiso.id).all()

	def get_by_id(self, idR):

		return self.db.session.query(Permiso).filter(Permiso.id == idR).first()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Permiso).filter(Permiso.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def get_permisos_by_sesion(self, id_sesion, menu):

		return self.db.session.execute(text("Select tbl_permiso.acceder, tbl_permiso.listar, tbl_permiso.add, tbl_permiso.edit, tbl_permiso.delete from tbl_permiso inner join tbl_perfil on tbl_perfil.id = tbl_permiso.idreg_fk_perfil inner join tbl_menus on tbl_menus.id = tbl_permiso.idreg_fk_menu inner join tbl_usuario on tbl_usuario.idreg_fk_perfil = tbl_perfil.id inner join tbl_session on tbl_session.id_fk_usuario = tbl_usuario.id where tbl_session.id = :sesion and tbl_menus.seguridad = :menu"),{'sesion':id_sesion, 'menu':menu})

	def get_permisos_for_listar(self):

		return self.db.session.execute(text("Select tbl_permiso.id, tbl_permiso.idreg_fk_menu, tbl_permiso.idreg_fk_perfil, tbl_permiso.acceder, tbl_permiso.listar, tbl_permiso.add, tbl_permiso.edit, tbl_permiso.delete, tbl_perfil.name_perfil, tbl_menus.name_menu from tbl_permiso inner join tbl_menus on tbl_menus.id = tbl_permiso.idreg_fk_menu inner join tbl_perfil on tbl_perfil.id = tbl_permiso.idreg_fk_perfil"))