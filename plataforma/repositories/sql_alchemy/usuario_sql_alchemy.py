from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError
from sqlalchemy import desc, asc
from sqlalchemy import and_
from sqlalchemy import func, text

from sqlalchemy import cast, Date

from plataforma.entities.usuario import Usuario

from plataforma.repositories.i_repository_usuario import IUsuarioRepository

class UsuarioRepositorySqlAlchemy(IUsuarioRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, usuario: Usuario):

		try:
			self.db.session.add(usuario)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()


	def get_all(self):

		return self.db.session.query(Usuario).order_by(Usuario.id).all()

	def get_by_id(self, id_usuario):

		return self.db.session.query(Usuario).filter(Usuario.id == id_usuario).first()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Usuario).filter(Usuario.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def get_usuario_by_username_login(self, usuario):

		return self.db.session.query(Usuario).filter(Usuario.user_name == usuario, Usuario.active == "s").first()

	def get_usuario_by_username_registro(self, usuario):

		return self.db.session.query(Usuario).filter(Usuario.user_name == usuario).first()

	def get_usuario_by_id(self, oid):

		return self.db.session.query(Usuario).filter(Usuario.id == oid).first()

	def get_usuario_by_usermail_registro(self, correo):

		return self.db.session.query(Usuario).filter(Usuario.user_mail == correo).first()

	def listar_usuarios(self):

		return self.db.session.execute(text("SELECT tbl_usuario.id, tbl_usuario.idreg_fk_perfil, tbl_usuario.idreg_fk_cliente, tbl_usuario.name, tbl_usuario.user_name, tbl_usuario.mobile ,tbl_usuario.user_mail, tbl_perfil.name_perfil, tbl_cliente.name, tbl_password.id from tbl_usuario inner join tbl_perfil on tbl_perfil.id = tbl_usuario.idreg_fk_perfil inner join tbl_cliente on tbl_cliente.id = tbl_usuario.idreg_fk_cliente inner join tbl_password on tbl_password.id_fk_usuario = tbl_usuario.id"))

