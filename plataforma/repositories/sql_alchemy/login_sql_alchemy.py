from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from plataforma.entities.login import Login

from plataforma.repositories.i_repository_login import ILoginRepository

class LoginRepositorySqlAlchemy(ILoginRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, login: Login):

		try:
			self.db.session.add(login)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Login).filter(Login.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()