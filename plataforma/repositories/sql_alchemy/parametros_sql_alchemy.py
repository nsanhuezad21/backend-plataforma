from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from plataforma.entities.parametros import Parametros

from plataforma.repositories.i_repository_parametros import IParametrosRepository

class ParametrosRepositorySqlAlchemy(IParametrosRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, parametro: Parametros):

		try:
			self.db.session.add(parametros)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()


	def get_all(self):

		return self.db.session.query(Parametros).order_by(Parametros.parameter_name).all()

	def get_by_name(self, name):

		return self.db.session.query(Parametros).filter(Parametros.parameter_name == name).first()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Parametros).filter(Parametros.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()