from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from plataforma.entities.password import Password

from plataforma.repositories.i_repository_password import IPasswordRepository

class PasswordRepositorySqlAlchemy(IPasswordRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, passw: Password):

		try:
			self.db.session.add(passw)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()


	def get_all(self):

		return self.db.session.query(Password).order_by(Password.id).all()

	def get_by_usuario(self, id_usuario):

		return self.db.session.query(Password).filter(Password.id_fk_usuario == id_usuario).first()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Password).filter(Password.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def delete_by_user(self, mid: int):

		try:
			self.db.session.query(Password).filter(Password.id_fk_usuario == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()