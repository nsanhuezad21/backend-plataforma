from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from plataforma.entities.perfiles import Perfil

from plataforma.repositories.i_repository_perfiles import IPerfilRepository

class PerfilRepositorySqlAlchemy(IPerfilRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, perfil: Perfil):

		try:
			self.db.session.add(perfil)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()


	def get_all(self):

		return self.db.session.query(Perfil).order_by(Perfil.id).all()

	def get_by_id(self, oid):

		return self.db.session.query(Perfil).filter(Perfil.id == oid).first()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Perfil).filter(Perfil.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()