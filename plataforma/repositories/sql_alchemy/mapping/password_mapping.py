import datetime

from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String, Boolean
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.password import Password
from plataforma.entities.usuario import Usuario

def password_mapping(metadata: MetaData):
	password = Table(
		'tbl_password',
		metadata,
		Column('id', Integer, Sequence('password_id_seq'), primary_key=True, nullable=False),
		Column('id_fk_usuario', Integer, nullable=False),
		Column('intentos', Integer, nullable=False),
		Column('bloqueado', Boolean, nullable=False),
		Column('fecha_bloqueo', DateTime, nullable=False),
        Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, default=datetime.datetime.now, nullable=False),
		info={'bind_key': 'plt'}
	)
	mapper(Password, password)

	return password