from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.menus import Menus

def menu_mapping(metadata: MetaData):
	menu = Table(
		'tbl_menus',
		metadata,
		Column('id', Integer, Sequence('menus_id_seq'), primary_key=True, nullable=False),
		Column('name_menu', String(50), nullable=False),
		Column('icon', String(50), nullable=False),
		Column('raiz', Integer, nullable=False),
		Column('link', String(255), nullable=False),
		Column('seguridad', String(255), nullable=False),
		Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_udp', Integer, nullable=False),
		Column('audit_fec_udp', DateTime, nullable=False),
		info={'bind_key': 'plt'}
	)

	mapper(Menus, menu)

	return menu