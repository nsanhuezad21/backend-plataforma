from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.perfiles import Perfil

def perfil_mapping(metadata: MetaData):
	perfil = Table(
		'tbl_perfil',
		metadata,
		Column('id', Integer, Sequence('perfil_id_seq'), primary_key=True, nullable=False),
		Column('name_perfil', String(500), nullable=False),
		Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_udp', Integer, nullable=False),
		Column('audit_fec_udp', DateTime, nullable=False),
		info={'bind_key': 'plt'}
	)

	mapper(Perfil, perfil)

	return perfil