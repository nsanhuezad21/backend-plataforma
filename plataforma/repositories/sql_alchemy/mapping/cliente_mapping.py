from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String, Boolean
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.cliente import Cliente

def cliente_mapping(metadata:MetaData):

    cliente= Table('tbl_cliente',
		metadata,
        Column('id', Integer, Sequence('cliente_id_seq'), primary_key=True, nullable=False),
		Column('name', String(250), nullable=False),
		Column('mobile', String(250), nullable=True),
		Column('office', String(500), nullable=True),
        Column('photo', String(500), nullable=True),
        Column('mail', String(500), nullable=False, unique=True),
        Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_udp', Integer, nullable=False),
		Column('audit_fec_udp', DateTime, nullable=False),
		info={'bind_key': 'plt'}
    )

    mapper(Cliente, cliente)

    return cliente