import datetime

from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.sesiones import Session
from plataforma.entities.usuario import Usuario

def session_mapping(metadata: MetaData):
	session = Table(
		'tbl_session',
		metadata,
		Column('id', Integer, Sequence('sesiones_id_seq'), primary_key=True, nullable=False),
		Column('id_fk_usuario', Integer, nullable=False),
		Column('session_ip', String(15), nullable=False),
		Column('session_host', String(250), nullable=False),
		Column('session_navegador', String(50), nullable=False),
		Column('session_start_time', DateTime, nullable=False),
		Column('session_end_time', DateTime, nullable=True),
		Column('session_time', Integer, nullable=True),
		Column('active', String(1), nullable=False),
		Column('audit_fec_ins', DateTime, default=datetime.datetime.now, nullable=False),
		info={'bind_key': 'plt'}
	)
	mapper(Session, session)

	return session