from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String, Boolean
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.usuario import Usuario
from plataforma.entities.perfiles import Perfil
from plataforma.entities.cliente import Cliente

def usuario_mapping(metadata:MetaData):

    usuario= Table('tbl_usuario',
		metadata,
        Column('id', Integer, Sequence('usuario_id_seq'), primary_key=True, nullable=False),
        Column('idreg_fk_perfil', Integer, ForeignKey('tbl_perfil.id'), nullable=False),
		Column('idreg_fk_cliente', Integer, ForeignKey('tbl_cliente.id'), nullable=False),
		Column('user_name', String(250), nullable=False, unique=True),
		Column('name', String(250), nullable=False),
		Column('mobile', String(250), nullable=True),
		Column('home', String(500), nullable=True),
		Column('office', String(500), nullable=True),
        Column('gender', String(1), nullable=False),
        Column('password', String(500), nullable=False),
        Column('empresa', String(500), nullable=True),
        Column('photo', String(500), nullable=True),
        Column('user_mail', String(500), nullable=False, unique=True),
        Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_udp', Integer, nullable=False),
		Column('audit_fec_udp', DateTime, nullable=False),
		info={'bind_key': 'plt'}
    )

    mapper(Usuario, usuario, properties={
		'tbl_perfil':relationship(Perfil,backref='usuario', order_by=usuario.c.id),
		'tbl_cliente':relationship(Cliente,backref='usuario', order_by=usuario.c.id)
	})

    return usuario