from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.parametros import Parametros

def parametro_mapping(metadata: MetaData):
	parametro = Table(
		'tbl_parametros',
		metadata,
		Column('id', Integer, Sequence('parametros_id_seq'), primary_key=True, nullable=False),
		Column('parameter_name', String(500), nullable=False),
		Column('desc_parameter', String(500), nullable=False),
		Column('value_parameter', String(255), nullable=False),
		Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_udp', Integer, nullable=False),
		Column('audit_fec_udp', DateTime, nullable=False),
		info={'bind_key': 'plt'}
	)

	mapper(Parametros, parametro)

	return parametro