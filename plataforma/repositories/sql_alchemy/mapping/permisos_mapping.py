from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String, Boolean
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.permisos import Permiso
from plataforma.entities.perfiles import Perfil
from plataforma.entities.menus import Menus

def permiso_mapping(metadata:MetaData):

    permiso= Table('tbl_permiso',
		metadata,
        Column('id', Integer, Sequence('permiso_id_seq'), primary_key=True, nullable=False),
        Column('idreg_fk_perfil', Integer, ForeignKey('tbl_perfil.id'), nullable=False),
        Column('idreg_fk_menu', Integer, ForeignKey('tbl_menus.id'), nullable=False),
		Column('acceder', Boolean, nullable=False),
		Column('listar', Boolean, nullable=False),
		Column('add', Boolean, nullable=False),
		Column('edit', Boolean, nullable=False),
		Column('delete', Boolean, nullable=False),
        Column('active', String(1), nullable=False),
		Column('audit_log_ins', Integer, nullable=False),
		Column('audit_fec_ins', DateTime, nullable=False),
		Column('audit_log_udp', Integer, nullable=False),
		Column('audit_fec_udp', DateTime, nullable=False),
		info={'bind_key': 'plt'}
    )

    mapper(Permiso, permiso, properties={
		'tbl_perfil':relationship(Perfil,backref='permiso', order_by=permiso.c.id),
		'tbl_menus':relationship(Menus,backref='permiso', order_by=permiso.c.id)
	})

    return permiso