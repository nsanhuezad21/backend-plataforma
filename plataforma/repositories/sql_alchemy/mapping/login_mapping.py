import datetime

from sqlalchemy import Table, MetaData, Column, Integer, Sequence, ForeignKey, DateTime, String
from sqlalchemy.orm import mapper, relationship

from plataforma.entities.login import Login

def login_mapping(metadata: MetaData):
	login = Table(
		'tbl_login',
		metadata,
		Column('id', Integer, Sequence('login_id_seq'), primary_key=True, nullable=False),
		Column('id_fk_user', Integer, nullable=False),
		Column('date_log', DateTime, nullable=False),
		Column('active', String(1), nullable=False),
		info={'bind_key': 'plt'}
	)
	mapper(Login, login)

	return login