from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError
from sqlalchemy import desc, asc
from sqlalchemy import and_
from sqlalchemy import func, text

from sqlalchemy import cast, Date

from plataforma.entities.cliente import Cliente

from plataforma.repositories.i_repository_cliente import IClienteRepository

class ClienteRepositorySqlAlchemy(IClienteRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, cliente: Cliente):

		try:
			self.db.session.add(cliente)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Cliente).filter(Cliente.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def get_all(self):

		return self.db.session.query(Cliente).order_by(Cliente.id).all()