from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError
from sqlalchemy import desc, asc
from sqlalchemy import and_
from sqlalchemy import func, text

from sqlalchemy import cast, Date

from plataforma.entities.menus import Menus

from plataforma.repositories.i_repository_menus import IMenuRepository

class MenuRepositorySqlAlchemy(IMenuRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, menu: Menus):

		try:
			self.db.session.add(menu)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def get_actives(self):

		return self.db.session.query(Menus).filter(Menus.active == 's').all()

	def get_by_id(self, idR):

		return self.db.session.query(Menus).filter(Menus.id == idR).first()

	def get_all(self):

		return self.db.session.query(Menus).order_by(Menus.id).all()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Menus).filter(Menus.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()

	def get_menus_nav(self, id_sesion):

		return self.db.session.execute(text("Select tbl_menus.id, tbl_menus.name_menu, tbl_menus.icon, tbl_menus.raiz, tbl_menus.link, tbl_menus.seguridad from tbl_menus inner join tbl_permiso on tbl_menus.id = tbl_permiso.idreg_fk_menu inner join tbl_usuario on tbl_permiso.idreg_fk_perfil = tbl_usuario.idreg_fk_perfil inner join tbl_session on tbl_usuario.id = tbl_session.id_fk_usuario where tbl_session.id = :sesion and tbl_menus.active = 's' and tbl_permiso.acceder = True"),{'sesion':id_sesion})