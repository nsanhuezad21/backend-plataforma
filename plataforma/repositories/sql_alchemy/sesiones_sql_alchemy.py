from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import IntegrityError

from plataforma.entities.sesiones import Session

from plataforma.repositories.i_repository_sesiones import ISessionRepository

class SesionRepositorySqlAlchemy(ISessionRepository):

	@inject
	def __init__(self, db: SQLAlchemy):

		self.db = db

	def add(self, sesion: Session):

		try:
			self.db.session.add(sesion)
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()


	def get_all(self):

		return self.db.session.query(Session).order_by(Session.name_menu).all()

	def get_by_id(self, id_sesion):

		return self.db.session.query(Session).filter(Session.id == id_sesion).first()

	def update(self):

		self.db.session.commit()

	def delete(self, mid: int):

		try:
			self.db.session.query(Session).filter(Session.id == mid).delete()
			self.db.session.commit()
		except IntegrityError:
			self.db.session.rollback()