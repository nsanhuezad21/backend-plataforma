from abc import ABC, abstractmethod

from plataforma.entities.usuario import Usuario

class IUsuarioRepository(ABC):

	@abstractmethod
	def add(self, usuario: Usuario):

		pass

	@abstractmethod
	def get_all(self):

		pass