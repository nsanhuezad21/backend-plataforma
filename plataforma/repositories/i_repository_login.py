from abc import ABC, abstractmethod

from plataforma.entities.login import Login

class ILoginRepository(ABC):

	@abstractmethod
	def add(self, login: Login):

		pass
