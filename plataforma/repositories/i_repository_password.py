from abc import ABC, abstractmethod

from plataforma.entities.password import Password

class IPasswordRepository(ABC):

	@abstractmethod
	def add(self, password: Password):

		pass

	@abstractmethod
	def get_all(self):

		pass