from abc import ABC, abstractmethod

from plataforma.entities.cliente import Cliente

class IClienteRepository(ABC):

	@abstractmethod
	def add(self, cliente: Cliente):

		pass

	@abstractmethod
	def get_all(self):

		pass