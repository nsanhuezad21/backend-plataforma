from abc import ABC, abstractmethod

from plataforma.entities.perfiles import Perfil

class IPerfilRepository(ABC):

	@abstractmethod
	def add(self, perfil: Perfil):

		pass

	@abstractmethod
	def get_all(self):

		pass