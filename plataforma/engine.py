from sqlalchemy import create_engine

is_production = False

if is_production:
    engine_connection_string = 'postgres://postgres:postgres@:5432/test'
else:
    engine_connection_string = 'postgres://postgres:postgres@:5432/test'

engine = create_engine(engine_connection_string, echo=False)
