import datetime

from flask import (Flask, g, render_template, flash,
                   url_for, redirect, abort, request, jsonify)
from injector import inject
from flask.views import View

from plataforma.views import validar_sesion, obtener_usuario, verificar_permiso

from plataforma.entities.menus import Menus
from ofuscador import OfuscadorMenu
from encriptador import desencriptar_valor, encriptar_valor

from plataforma.use_cases.menu_use_case import MenuUseCase
from plataforma.use_cases.usuario_use_case import UsuarioUseCase
from plataforma.use_cases.perfil_use_case import PerfilUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase
from plataforma.use_cases.sesiones_use_case import SesionUseCase


class Menus_View(View):

    @inject
    def __init__(self, muc: MenuUseCase, uuc: UsuarioUseCase, puc: PerfilUseCase, pruc: PermisoUseCase, suc: SesionUseCase):
        
        self.muc = muc
        self.uuc = uuc
        self.puc = puc
        self.pruc = pruc
        self.suc = suc

    def menus(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.args.get("menu")
        datos = OfuscadorMenu
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "acceder")
            if permiso == True:
                permisos = self.pruc.get_permiso_by_session(sesion, menu)
                return render_template('menus/menus.html', sesion = encriptar_valor(sesion), datos =datos, menu = menu, permisos = permisos)
            else:
                return redirect(url_for('plt_adelaida.menu_principal')+"?sesion="+encriptar_valor(sesion))
        else:
            return redirect(url_for('plt_adelaida.login'))

    def listar_menus(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        menu = request.args.get("menu")
        validado = validar_sesion(sesion, self.suc)
        lista = []
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "listar")
            if permiso == True:
                menus = self.muc.get_all()
                try:
                    for m in menus:
                        lista.append({ "id": m.id, "name": m.name_menu, "icon": m.icon, "raiz": m.raiz, "link": m.link, "seguridad": m.seguridad, "active": m.active})
                except:
                    return "Algo Fallo", 500
                return jsonify(lista)
            else:
                return "No tienes permiso para acceder a esta informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))
        
    def add_menus(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        menu = request.form.get("menu")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "add")
            if permiso == True:
                name = request.form.get(OfuscadorMenu[0].ofuscador)
                icon = request.form.get(OfuscadorMenu[1].ofuscador)
                raiz = request.form.get(OfuscadorMenu[2].ofuscador)
                link = request.form.get(OfuscadorMenu[3].ofuscador)
                seguridad = request.form.get(OfuscadorMenu[4].ofuscador)

                oMenu = Menus(name, icon, int(raiz), link, seguridad, 'n', 1, 1)

                try:
                    self.muc.add(oMenu)
                    return "Se agrego el menu exitosamente", 200
                except:
                    return "Algo Fallo al guardar el menu", 500
            else:
                return "No tienes permiso para agregar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def cmb_menus(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        lista = []
        if validado == True:
            menus = self.muc.get_all()
            try:
                for m in menus:
                    lista.append({ "id": m.id, "name": m.name_menu})
            except:
                return "Algo Fallo"
            return jsonify(lista)
        else:
            return redirect(url_for('plt_adelaida.login'))

    def delete_menu(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        idR = request.form.get("id")
        menu = request.form.get("menu")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "delete")
            if permiso == True:
                self.muc.delete_menu_trace(int(idR))
                return "Permiso borrado exitosamente", 200
            else:
                return "No tienes permiso para borrar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def edit_menu(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.form.get("menu")
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "edit")
            if permiso == True:
                idR = request.form.get("id")
                oMenu = self.muc.get_by_id(int(idR))
                if oMenu != None:
                    oMenu.name_menu = request.form.get(OfuscadorMenu[0].ofuscador)
                    oMenu.icon = request.form.get(OfuscadorMenu[1].ofuscador)
                    oMenu.raiz = int(request.form.get(OfuscadorMenu[2].ofuscador))
                    oMenu.link = request.form.get(OfuscadorMenu[3].ofuscador)
                    oMenu.seguridad = request.form.get(OfuscadorMenu[4].ofuscador)
                    self.muc.update()
                    return "Se edito exitosamente", 200
                else:
                    return "algo fallo al editar", 500
            else:
                return "No tienes permiso para editar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def menu_nav(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        lista = []
        if validado == True:
            menus = self.muc.get_menus_nav(sesion)
            if menus.rowcount >= 1:
                for m in menus:
                    lista.append({"id": m[0], "name": m[1], "icono": m[2], "raiz": m[3], "link": m[4], "seguridad": m[5]})
                return jsonify(lista)
            else:
                return "no tienes permisos para acceder a ningun menu", 500
        else:
            return redirect(url_for('plt_adelaida.login'))