import datetime

from flask import (Flask, g, render_template, flash,
                   url_for, redirect, abort, request, jsonify)
from injector import inject
from flask.views import View



from plataforma.entities.parametros import Parametros

from plataforma.use_cases.parametro_use_case import ParametroUseCase
from plataforma.use_cases.usuario_use_case import UsuarioUseCase
from plataforma.use_cases.perfil_use_case import PerfilUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase


class Parametro_View(View):

    @inject
    def __init__(self, ptuc: ParametroUseCase, uuc: UsuarioUseCase, puc: PerfilUseCase, pruc = PermisoUseCase):
        self.ptuc = ptuc
        self.uuc = uuc
        self.puc = puc
        self.pruc = pruc

    def parametro(self, id_menu):

        try:
            if permiso.acceder == True:
                return render_template('parametro/parametro.html')
            else:
                pass
        except:
            pass
        