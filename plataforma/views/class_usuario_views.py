import datetime

from flask import (Flask, g, render_template, flash,
                   url_for, redirect, abort, request, jsonify)
from injector import inject
from flask.views import View

from plataforma.views import validar_sesion, verificar_permiso

from plataforma.entities.usuario import Usuario
from plataforma.entities.password import Password
from encriptador import desencriptar_valor, encriptar_valor

from plataforma.use_cases.usuario_use_case import UsuarioUseCase
from plataforma.use_cases.perfil_use_case import PerfilUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase
from plataforma.use_cases.sesiones_use_case import SesionUseCase
from plataforma.use_cases.cliente_use_case import ClienteUseCase
from plataforma.use_cases.password_use_case import PasswordUseCase
from ofuscador import OfuscadorLogin, OfuscadorUsuario



class Usuario_View(View):

    @inject
    def __init__(self, uuc: UsuarioUseCase, puc: PerfilUseCase, pruc: PermisoUseCase, suc : SesionUseCase, cluc: ClienteUseCase, pwuc: PasswordUseCase):

        self.uuc = uuc
        self.puc = puc
        self.pruc = pruc
        self.suc = suc
        self.cluc = cluc
        self.pwuc = pwuc

    def usuario(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.args.get("menu")
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "acceder")
            if permiso == True:
                datos = OfuscadorUsuario
                empresas = self.cluc.get_all()
                perfil = self.puc.get_all()
                return render_template('usuario/usuario.html', sesion = encriptar_valor(sesion), datos = datos, menu = menu, empresas = empresas, perfil = perfil)
            else:
                return redirect(url_for('plt_adelaida.menu_principal')+"?sesion="+encriptar_valor(sesion))
        else:
            return redirect(url_for('plt_adelaida.login'))

    def listar_usuario(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        lista = []
        menu = request.args.get("menu")
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "listar")
            if permiso == True:
                usuarios = self.uuc.listar_usuario()
                try:
                    for u in usuarios:
                        lista.append({"id": u[0], "perfil": u[1], "cliente": u[2], "nombre": u[3], "user_name": u[4], "mobile": u[5], "user_mail": u[6], "perfil_n": u[7], "cliente_n": u[8], "pw_id": u[9]})
                    return jsonify(lista)
                except:
                    return "Algo Fallo", 500
            else:
                return "No tienes permiso para acceder a esta informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def validar_username(self):

        usuario = request.form.get(OfuscadorUsuario[2].ofuscador)
        registro = self.uuc.get_usuario_by_username_registro(usuario)
        if registro != None:
            return "EL usuario ya existe", 500
        else:
            return "Usuario Disponible", 200

    def validar_email(self):

        correo = request.form.get(OfuscadorUsuario[11].ofuscador)
        registro = self.uuc.get_usuario_by_usermail_registro(correo)
        if registro != None:
            return "El correo ya esta en uso", 500
        else:
            return "Correo disponible", 200

    def add_user(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        menu = request.form.get("menu")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "add")
            if permiso == True:

                oUser = Usuario(
                    idreg_fk_perfil = int(request.form.get(OfuscadorUsuario[0].ofuscador)),
                    idreg_fk_cliente = int(request.form.get(OfuscadorUsuario[1].ofuscador)),
                    user_name = request.form.get(OfuscadorUsuario[2].ofuscador),
                    name = request.form.get(OfuscadorUsuario[3].ofuscador),
                    mobile = request.form.get(OfuscadorUsuario[4].ofuscador),
                    home="sin especificar",
                    office="sin especificar",
                    gender='o',
                    password=request.form.get(OfuscadorUsuario[8].ofuscador),
                    empresa="",
                    photo="",
                    user_mail= request.form.get(OfuscadorUsuario[11].ofuscador),
                    active='s',
                    audit_log_ins = 1,
                    audit_log_udp = 1
                )

                try:
                    self.uuc.add(oUser)

                    oPass = Password(oUser.id, 0, False, datetime.datetime.now(), oUser.id)

                    self.pwuc.add(oPass)

                    return "Se agrego el menu exitosamente", 200
                except:
                    return "Algo Fallo al guardar el menu", 500
            else:
                return "No tienes permiso para agregar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def edit_user(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.form.get("menu")
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "edit")
            if permiso == True:
                idR = request.form.get("id")
                oUser = self.uuc.get_usuario_by_id(int(idR))
                if oUser != None:
                    oUser.idreg_fk_perfil = int(request.form.get(OfuscadorUsuario[0].ofuscador))
                    oUser.idreg_fk_cliente = int(request.form.get(OfuscadorUsuario[1].ofuscador))
                    oUser.user_name = request.form.get(OfuscadorUsuario[2].ofuscador)
                    oUser.name = request.form.get(OfuscadorUsuario[3].ofuscador)
                    oUser.mobile = request.form.get(OfuscadorUsuario[4].ofuscador)
                    oUser.user_mail = request.form.get(OfuscadorUsuario[11].ofuscador)
                    self.uuc.update()
                    return "Se edito exitosamente", 200
                else:
                    return "algo fallo al editar", 500
            else:
                return "No tienes permiso para editar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def delete_user(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        idR = request.form.get("id")
        menu = request.form.get("menu")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "delete")
            if permiso == True:
                try:
                    self.uuc.delete(int(idR))
                    self.pwuc.delete_by_password(int(idR))
                    return "Permiso borrado exitosamente", 200
                except:
                    return "Algo fallo", 500
            else:
                return "No tienes permiso para borrar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))
