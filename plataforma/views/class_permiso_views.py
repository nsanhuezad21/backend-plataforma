import datetime

from flask import (Flask, g, render_template, flash,
                   url_for, redirect, abort, request, jsonify)
from injector import inject
from flask.views import View
from plataforma.views import validar_sesion, verificar_permiso
from ofuscador import OfuscadorPermiso

from plataforma.entities.permisos import Permiso
from encriptador import desencriptar_valor, encriptar_valor

from plataforma.use_cases.usuario_use_case import UsuarioUseCase
from plataforma.use_cases.perfil_use_case import PerfilUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase
from plataforma.use_cases.sesiones_use_case import SesionUseCase
from plataforma.use_cases.menu_use_case import MenuUseCase

def convertir_boolean(valor):

    if valor == "true":
        return True
    else:
        return False

class Permiso_View(View):

    @inject
    def __init__(self, uuc: UsuarioUseCase, puc: PerfilUseCase, pruc : PermisoUseCase, suc: SesionUseCase, muc: MenuUseCase):

        self.uuc = uuc
        self.puc = puc
        self.pruc = pruc
        self.suc = suc
        self.muc = muc

    def Permiso(self):
        
        sesion = desencriptar_valor(request.args.get("sesion"))
        menu = request.args.get("menu")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "acceder")
            if permiso == True:
                menus = self.muc.get_all()
                perfil = self.puc.get_all()
                datos = OfuscadorPermiso
                return render_template('permisos/permisos.html', sesion = encriptar_valor(sesion), datos = datos, menus = menus, perfil = perfil, menu = menu)
            else:
                return redirect(url_for('plt_adelaida.menu_principal')+"?sesion="+encriptar_valor(sesion))
        else:
            return redirect(url_for('plt_adelaida.login'))

    def listar_permiso(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.args.get("menu")
        lista = []
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "listar")
            if permiso == True:
                permisos = self.pruc.get_permisos_for_listar()
                try:
                    for pe in permisos:
                        lista.append({"id": pe[0], "perfil": pe[2], "menu": pe[1], "acceder": pe[3], "listar": pe[4], "add": pe[5], "edit": pe[6], "delete": pe[7], "perfil_n": pe[8], "menu_n": pe[9]})
                except:
                    return "Algo Fallo", 500
                return jsonify(lista)
            else:
                return "No tienes permiso para acceder a esta informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def add_permiso(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.form.get("menu")
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "add")
            if permiso == True:
                perfil = request.form.get(OfuscadorPermiso[0].ofuscador)
                menu = request.form.get(OfuscadorPermiso[1].ofuscador)
                acceder = convertir_boolean(request.form.get(OfuscadorPermiso[2].ofuscador))
                listar = convertir_boolean(request.form.get(OfuscadorPermiso[3].ofuscador))
                agregar = convertir_boolean(request.form.get(OfuscadorPermiso[4].ofuscador))
                editar = convertir_boolean(request.form.get(OfuscadorPermiso[5].ofuscador))
                borrar = convertir_boolean(request.form.get(OfuscadorPermiso[6].ofuscador))

                oPermiso = Permiso(int(perfil), int(menu), acceder, listar, agregar, editar, borrar, 's', 1, 1)

                self.pruc.add(oPermiso)

                return "", 200
            else:
                return "No tienes permiso para agregar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def cambio_estado(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        idR = request.form.get("id")
        menu = request.form.get("menu")
        valor = convertir_boolean(request.form.get("valor"))
        opcion = request.form.get("opcion")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "edit")
            if permiso == True:
                oPermiso = self.pruc.get_by_id(int(idR))
                if opcion == 'acceder':
                    if valor == True:
                        oPermiso.acceder = False
                    else:
                        oPermiso.acceder = True
                
                elif opcion == 'listar':
                    if valor == True:
                        oPermiso.listar = False
                    else:
                        oPermiso.listar = True

                elif opcion == 'add':
                    if valor == True:
                        oPermiso.add = False
                    else:
                        oPermiso.add = True

                elif opcion == 'edit':
                    if valor == True:
                        oPermiso.edit = False
                    else:
                        oPermiso.edit = True

                elif opcion == 'delete':
                    if valor == True:
                        oPermiso.delete = False
                    else:
                        oPermiso.delete = True

                self.pruc.update()
                return "Se cambio el estado", 200
            else:
                return "No tienes permiso para editar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def delete_permiso(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        idR = request.form.get("id")
        menu = request.form.get("menu")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "delete")
            if permiso == True:
                self.pruc.delete(int(idR))
                return "Permiso borrado exitosamente", 200
            else:
                return "No tienes permiso para borrar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))
        