import datetime

from flask import (Flask, g, render_template, flash,
                   url_for, redirect, abort, request, jsonify)
from injector import inject
from flask.views import View

from plataforma.views import validar_sesion, verificar_permiso

from plataforma.entities.perfiles import Perfil
from encriptador import desencriptar_valor, encriptar_valor

from plataforma.use_cases.usuario_use_case import UsuarioUseCase
from plataforma.use_cases.perfil_use_case import PerfilUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase
from plataforma.use_cases.sesiones_use_case import SesionUseCase

from ofuscador import OfuscadorPerfil


class Perfil_View(View):

    @inject
    def __init__(self, uuc: UsuarioUseCase, puc: PerfilUseCase, pruc: PermisoUseCase, suc: SesionUseCase):

        self.uuc = uuc
        self.puc = puc
        self.pruc = pruc
        self.suc = suc

    def perfil(self):
        
        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        datos = OfuscadorPerfil
        menu = request.args.get("menu")
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "acceder")
            if permiso == True:
                return render_template('perfil/perfil.html', sesion = encriptar_valor(sesion), datos = datos, menu = menu)
            else:
                return redirect(url_for('plt_adelaida.menu_principal')+"?sesion="+encriptar_valor(sesion))
        else:
            return redirect(url_for('plt_adelaida.login'))

    def listar_perfil(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.args.get("menu")
        lista = []
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "listar")
            if permiso == True:
                perfiles = self.puc.get_all()
                if perfiles != None:
                    for p in perfiles:
                        lista.append({ "id": p.id, "name": p.name_perfil, "active": p.active})
                    return jsonify(lista)
                else:
                    return "No hay registros en la base de datos", 500
            else:
                return "no tienes permiso para ver esta informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

    def add_perfil(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        menu = request.form.get("menu")
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "add")
            if permiso == True:
                name =  request.form.get(OfuscadorPerfil[0].ofuscador)

                oPerfil = Perfil(name, 's', 1, 1)

                try:
                    self.puc.add(oPerfil)
                    return "Se guardo el perfil exitosamente", 200
                except:
                    return "Algo fallo", 500
            else:
                return "No tienes permiso para agregar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))
        
    def delete_perfil(self):

        sesion = desencriptar_valor(request.form.get("sesion"))
        idR = request.form.get("id")
        menu = request.form.get("menu")
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "delete")
            if permiso == True:
                self.puc.delete(int(idR))
                return "Perfil borrado exitosamente", 200
            else:
                return "No tienes permiso para borrar informacion", 500
        else:
            return redirect(url_for('plt_adelaida.login'))

