import datetime

from flask import (Flask, g, render_template, flash,
                   url_for, redirect, abort, request, jsonify)
from injector import inject
from flask.views import View
from flask_bcrypt import check_password_hash, generate_password_hash

from plataforma.entities.usuario import Usuario
from plataforma.entities.password import Password
from plataforma.entities.sesiones import Session
from plataforma.entities.login import Login
from plataforma.views import validar_sesion

from encriptador import desencriptar_valor, encriptar_valor

# sesion se eliminan y se debe validar si existe ese id
from plataforma.use_cases.usuario_use_case import UsuarioUseCase
from plataforma.use_cases.perfil_use_case import PerfilUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase
from plataforma.use_cases.password_use_case import PasswordUseCase
from plataforma.use_cases.sesiones_use_case import SesionUseCase
from plataforma.use_cases.login_use_case import LoginUseCase
from plataforma.use_cases.parametro_use_case import ParametroUseCase
from ofuscador import OfuscadorLogin, OfuscadorUsuario

class Login_View(View):

    @inject
    def __init__(self, uuc: UsuarioUseCase, puc: PerfilUseCase, pruc : PermisoUseCase, pauc: PasswordUseCase, suc: SesionUseCase, luc: LoginUseCase, ptuc: ParametroUseCase):

        self.uuc = uuc
        self.puc = puc
        self.pruc = pruc
        self.pauc = pauc
        self.suc = suc
        self.luc = luc
        self.ptuc = ptuc

    def login(self):
        
        datos = OfuscadorLogin
        return render_template('login.html', datos = datos)

    def ingreso(self):

        usuario = request.form.get(OfuscadorLogin[0].ofuscador)
        limite_errores = self.ptuc.get_by_name("limite_fallos")
        contraseña = request.form.get(OfuscadorLogin[1].ofuscador)
        registro = self.uuc.get_usuario_by_username_login(usuario)
        ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
        agente = request.user_agent.string
        navegador = request.user_agent.browser
        sesion_id = []
        if (registro):
            pass_registro = self.pauc.get_by_usuario(registro.id)
            if pass_registro:
                validado = check_password_hash(registro.password, contraseña)
                if validado == True:
                    if pass_registro.bloqueado != True:

                        pass_registro.intentos = 0
                        self.pauc.update()
                        # se crea la sension del usuario y se manda al front
                        
                        oSesion = Session( registro.id, ip, agente , navegador, datetime.datetime.now(), None, None, "s", datetime.datetime.now())
                        self.suc.add(oSesion)
                        #por front se manda dos veces esta peticion corregir para que la sesion se cree al cargar menu principal
                        id_sesion = encriptar_valor(oSesion.id)
                        sesion_id.append({"sesion": id_sesion})

                        #se crea el registro de ingreso en la tabla login

                        oLogin = Login(registro.id, datetime.datetime.now(), "s")
                        self.luc.add(oLogin)

                        return jsonify(sesion_id)
                        # return "verificado", 200
                    else:
                        return "Usuario bloqueado", 500
                    
                else:
                    # contar los errores y bloquear al tercer intento
                    intentos = pass_registro.intentos
                    
                    if intentos >= int(limite_errores.value_parameter):
                        pass_registro.bloqueado = True
                        self.pauc.update()
                        return "El usuario se a bloqueado", 500
                    else:
                        intentos += 1
                        pass_registro.intentos = intentos
                        self.pauc.update()
                        return "Contraseña equivocada", 500
            else:
                return "Usuario no encontrado", 500
        else:
            return "Usuario no encontrado", 500

    def menu_principal(self):
        sesion = desencriptar_valor(request.args.get("sesion"))
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            return render_template('index.html', sesion = encriptar_valor(sesion))
        else:
            return redirect(url_for('plt_adelaida.login'))
            
    def registro(self):

        datos = OfuscadorUsuario

        return render_template('registro.html', datos = datos)
        
    def registrar(self):

        try:
            usuario = request.form.get(OfuscadorUsuario[2].ofuscador)
            nombre = request.form.get(OfuscadorUsuario[3].ofuscador)
            telefono = request.form.get(OfuscadorUsuario[4].ofuscador)
            direccion = request.form.get(OfuscadorUsuario[5].ofuscador)
            oficina = request.form.get(OfuscadorUsuario[6].ofuscador)
            # genero = request.form.get(OfuscadorUsuario[7].ofuscador)
            contraseña = request.form.get(OfuscadorUsuario[8].ofuscador)
            correo = request.form.get(OfuscadorUsuario[11].ofuscador)

            oUser = Usuario( 1, 1, usuario , nombre, telefono, direccion, oficina, "o", contraseña, "", "", correo, "s", 1, 1)

            self.uuc.add(oUser)

            oPass = Password(oUser.id, 0, False, datetime.datetime.now(), oUser.id)

            self.pauc.add(oPass)

            return "", 200
        except:
            return "", 500

    def logout(self):

        sesion = desencriptar_valor(request.args.get("sesion"))
        self.suc.delete(int(sesion))
        return redirect(url_for('plt_adelaida.login'))
