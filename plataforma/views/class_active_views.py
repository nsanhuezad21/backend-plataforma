import datetime

from flask import (Flask, g, render_template, flash,
                   url_for, redirect, abort, request, jsonify)
from injector import inject
from flask.views import View
from flask_bcrypt import check_password_hash, generate_password_hash

from plataforma.entities.usuario import Usuario
from plataforma.entities.password import Password
from plataforma.entities.sesiones import Session
from plataforma.entities.login import Login
from plataforma.views import validar_sesion, verificar_permiso

from encriptador import desencriptar_valor, encriptar_valor

# sesion se eliminan y se debe validar si existe ese id
from plataforma.use_cases.active_use_case import ActiveUseCase
from plataforma.use_cases.sesiones_use_case import SesionUseCase
from plataforma.use_cases.permiso_use_case import PermisoUseCase

class Active_View(View):

    @inject
    def __init__(self, auc: ActiveUseCase, suc: SesionUseCase, pruc: PermisoUseCase):

        self.auc = auc
        self.suc = suc
        self.pruc = pruc

    def change_Active(self):
        sesion = desencriptar_valor(request.form.get('sesion'))
        menu = request.form.get('menu')
        validado = validar_sesion(sesion, self.suc)
        if validado == True:
            permiso = verificar_permiso(sesion, self.pruc, menu, "edit")
            if permiso == True:
                oId = request.form.get('id')
                entitie = request.form.get('entitie')
                oChange = self.auc.get_objeto(entitie, oId)
                if oChange != 'error':
                    if oChange.active == 's':
                        oChange.active = 'n'
                        oChange.audit_fec_upd = datetime.datetime.now()
                    else:
                        oChange.active = 's' 
                        oChange.audit_fec_upd = datetime.datetime.now()
                    self.auc.update(entitie)
                    return 'Listo', 200
                else:
                    return "ha ocurrido un error", 500
            else:
                return "No tienes permiso para editar", 500
        else:
            return redirect(url_for('plt_adelaida.login'))