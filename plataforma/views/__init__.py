import datetime

def validar_sesion(sesion, use_cases):
    sesion_registro = use_cases.get_by_id(sesion)
    try:
        if sesion_registro.active == "s":
            sesion_registro.session_start_time = datetime.datetime.now()
            use_cases.update()
            return True
        else:
            return False
    except:
        return False

def obtener_usuario(sesion, use_cases):

    rSesion = use_cases.get_by_id(sesion)
    try:
        return rSesion.id_fk_usuario
    except:
        return None

def verificar_permiso(sesion, use_cases, menu, accion):

    permiso = use_cases.get_permiso_by_session(sesion, menu)
    if permiso.rowcount >= 1:
        for p in permiso:
            if accion == "acceder":
                return p[0]
            elif accion == "listar":
                return p[1]
            elif accion == "add":
                return p[2]
            elif accion == "edit":
                return p[3]
            elif accion == "delete":
                return p[4]
            else:
                return False
    else:
        return False
        
    