from injector import inject

from plataforma.entities.menus import Menus

from plataforma.use_cases.i_use_case.i_menus_use_case import IMenuUseCase

from plataforma.repositories.sql_alchemy.menus_sql_alchemy import MenuRepositorySqlAlchemy

class MenuUseCase(IMenuUseCase):

	@inject
	def __init__(self, menu_repo: MenuRepositorySqlAlchemy):

		self.menu_repo = menu_repo

	def add(self, menu: Menus):

		self.menu_repo.add(menu)

	def update(self):
    		self.menu_repo.update()

	def get_all(self):

		return self.menu_repo.get_all()

	def delete_menu_trace(self, id):
		self.menu_repo.delete(id)
	
	def get_by_id(self, id):

		return self.menu_repo.get_by_id(id)

	def get_menus_nav(self, id_sesion):

		return self.menu_repo.get_menus_nav(id_sesion)

