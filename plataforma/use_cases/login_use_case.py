from injector import inject

from plataforma.entities.login import Login

from plataforma.use_cases.i_use_case.i_login_use_case import ILoginUseCase

from plataforma.repositories.sql_alchemy.login_sql_alchemy import LoginRepositorySqlAlchemy

class LoginUseCase(ILoginUseCase):

	@inject
	def __init__(self, login_repo: LoginRepositorySqlAlchemy):

		self.login_repo = login_repo

	def add(self, login: Login):

		self.login_repo.add(login)

	def update(self):
    	    self.login_repo.update()

	def delete_menu_trace(self, id):
		self.login_repo.delete(id)
		self.login_repo.update()

