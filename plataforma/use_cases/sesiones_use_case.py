from injector import inject

from plataforma.entities.sesiones import Session

from plataforma.use_cases.i_use_case.i_sesiones_use_case import ISesionUseCase

from plataforma.repositories.sql_alchemy.sesiones_sql_alchemy import SesionRepositorySqlAlchemy

class SesionUseCase(ISesionUseCase):

	@inject
	def __init__(self, sesion_repo: SesionRepositorySqlAlchemy):

		self.sesion_repo = sesion_repo

	def add(self, sesion: Session):

		self.sesion_repo.add(sesion)

	def update(self):
    		self.sesion_repo.update	()

	def get_all(self):

		return self.sesion_repo.get_all()

	def delete(self, id):
		self.sesion_repo.delete(id)

	def get_by_id(self, id_sesion):

		return self.sesion_repo.get_by_id(id_sesion)

