from injector import inject

from plataforma.entities.permisos import Permiso

from plataforma.use_cases.i_use_case.i_permisos_use_case import IPermisoUseCase

from plataforma.repositories.sql_alchemy.permiso_sql_alchemy import permisoRepositorySqlAlchemy

class PermisoUseCase(IPermisoUseCase):

	@inject
	def __init__(self, permiso_repo: permisoRepositorySqlAlchemy):

		self.permiso_repo = permiso_repo

	def add(self, permiso: Permiso):

		self.permiso_repo.add(permiso)

	def update(self):
    		self.permiso_repo.update()

	def get_all(self):

		return self.permiso_repo.get_all()

	def get_by_id(self, idR):

		return self.permiso_repo.get_by_id(idR)

	def delete(self, id):
		self.permiso_repo.delete(id)

	def get_permiso_by_session(self, id_sesion, menu):

		return self.permiso_repo.get_permisos_by_sesion(id_sesion, menu)

	def get_permisos_for_listar(self):

		return self.permiso_repo.get_permisos_for_listar()

