from injector import inject

from plataforma.entities.parametros import Parametros

from plataforma.use_cases.i_use_case.i_parametros_use_case import IParametroUseCase

from plataforma.repositories.sql_alchemy.parametros_sql_alchemy import ParametrosRepositorySqlAlchemy

class ParametroUseCase(IParametroUseCase):

	@inject
	def __init__(self, para_repo: ParametrosRepositorySqlAlchemy):

		self.para_repo = para_repo

	def add(self, parametro: Parametros):

		self.para_repo.add(parametro)

	def update(self):
    		self.para_repo.update()

	def get_all(self):

		return self.para_repo.get_all()

	def get_by_name(self, name):

		return self.para_repo.get_by_name(name)

	def delete(self, id):
		self.para_repo.delete(id)

