from injector import inject

from plataforma.entities.perfiles import Perfil

from plataforma.use_cases.i_use_case.i_perfil_use_case import IPerfilUseCase

from plataforma.repositories.sql_alchemy.perfiles_sql_alchemy import PerfilRepositorySqlAlchemy

class PerfilUseCase(IPerfilUseCase):

	@inject
	def __init__(self, perfil_repo: PerfilRepositorySqlAlchemy):

		self.perfil_repo = perfil_repo

	def add(self, perfil: Perfil):

		self.perfil_repo.add(perfil)

	def update(self):
    		self.perfil_repo.update()

	def get_all(self):

		return self.perfil_repo.get_all()

	def delete(self, id):
		self.perfil_repo.delete(id)

