from injector import inject

from plataforma.entities.usuario import Usuario

from plataforma.use_cases.i_use_case.i_usuario_use_case import IUsuarioUseCase

from plataforma.repositories.sql_alchemy.usuario_sql_alchemy import UsuarioRepositorySqlAlchemy

class UsuarioUseCase(IUsuarioUseCase):

	@inject
	def __init__(self, usuario_repo: UsuarioRepositorySqlAlchemy):

		self.usuario_repo = usuario_repo

	def add(self, usuario: Usuario):

		self.usuario_repo.add(usuario)

	def update(self):
    		self.usuario_repo.update()

	def get_all(self):

		return self.usuario_repo.get_all()

	def delete(self, id):
		self.usuario_repo.delete(id)

	def get_usuario_by_username_login(self, usuario):

		return self.usuario_repo.get_usuario_by_username_login(usuario)

	def get_usuario_by_username_registro(self, usuario):

		return self.usuario_repo.get_usuario_by_username_registro(usuario)

	def get_usuario_by_usermail_registro(self, correo):

		return self.usuario_repo.get_usuario_by_usermail_registro(correo)

	def get_usuario_by_id(self, id_usuario):

		return self.usuario_repo.get_by_id(id_usuario)

	def listar_usuario(self):

		return self.usuario_repo.listar_usuarios()

