from injector import inject

from plataforma.entities.cliente import Cliente

from plataforma.use_cases.i_use_case.i_cliente_use_case import IClienteUseCase

from plataforma.repositories.sql_alchemy.cliente_sql_alchemy import ClienteRepositorySqlAlchemy

class ClienteUseCase(IClienteUseCase):

    @inject
    def __init__(self, cliente_repo: ClienteRepositorySqlAlchemy):

        self.cliente_repo = cliente_repo

    def add(self, cliente: Cliente):

        self.cliente_repo.add(cliente)

    def update(self):
    	self.cliente_repo.update()

    def delete(self, mid):

        self.cliente_repo.delete(mid)

    def get_all(self):

        return self.cliente_repo.get_all()