from injector import inject

from plataforma.entities.password import Password

from plataforma.use_cases.i_use_case.i_password_use_case import IPasswordUseCase

from plataforma.repositories.sql_alchemy.password_sql_alchemy import PasswordRepositorySqlAlchemy

class PasswordUseCase(IPasswordUseCase):

	@inject
	def __init__(self, pass_repo: PasswordRepositorySqlAlchemy):

		self.pass_repo = pass_repo

	def add(self, password: Password):

		self.pass_repo.add(password)

	def update(self):
    		self.pass_repo.update()

	def get_all(self):

		return self.pass_repo.get_all()

	def delete(self, id):
		self.pass_repo.delete(id)

	def get_by_usuario(self, id_usuario):

		return self.pass_repo.get_by_usuario(id_usuario)

	def delete_by_password(self, id_r):

		self.pass_repo.delete_by_password(id_r)