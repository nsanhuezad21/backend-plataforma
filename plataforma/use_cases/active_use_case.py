from injector import inject

from plataforma.use_cases.i_use_case.i_active_use_case import IActiveUseCase

from plataforma.repositories.sql_alchemy.menus_sql_alchemy import MenuRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.perfiles_sql_alchemy import PerfilRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.permiso_sql_alchemy import permisoRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.usuario_sql_alchemy import UsuarioRepositorySqlAlchemy

class ActiveUseCase(IActiveUseCase):

	@inject
	def __init__(self, menu_repo: MenuRepositorySqlAlchemy, perfil_repo: PerfilRepositorySqlAlchemy, permiso_repo: permisoRepositorySqlAlchemy, usuario_repo: UsuarioRepositorySqlAlchemy):

		self.menu_repo = menu_repo
		self.perfil_repo = perfil_repo
		self.permiso_repo = permiso_repo
		self.usuario_repo = usuario_repo
	
	def get_objeto(self, entitie: str, oId:int):

		if entitie == 'usuario':
			return self.usuario_repo.get_usuario_by_id(oId)
		elif entitie == 'menu':
			return self.menu_repo.get_by_id(oId)
		elif entitie == 'perfil':
			return self.perfil_repo.get_by_id(oId)
		elif entitie == 'permiso':
			return self.permiso_repo.get_by_id(oId)
		else:
			return 'error'
	
	def update(self, entitie):

		if entitie == 'usuario':
			return self.usuario_repo.update()
		elif entitie == 'menu':
			return self.menu_repo.update()
		elif entitie == 'perfil':
			return self.perfil_repo.update()
		elif entitie == 'permiso':
			return self.permiso_repo.update()
		else:
			return 'error'