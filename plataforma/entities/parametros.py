import datetime

class Parametros:

	def __init__(self, parameter_name:str, desc_parameter:str, value_parameter:str, active:str, audit_log_ins: int, audit_log_udp: int):

		self.parameter_name = parameter_name
		self.desc_parameter = desc_parameter
		self.value_parameter = value_parameter
		self.active = active
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = self.date_now()
		self.audit_log_udp = audit_log_udp
		self.audit_fec_udp = self.date_now()

	def date_now(self):

		x = datetime.datetime.now()
		return x