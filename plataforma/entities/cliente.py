import datetime

from flask_bcrypt import generate_password_hash


class Cliente:
    def __init__(self, name=str, mobile=str, office=str, photo=str, mail=str, active='s', birthdate=str, audit_log_ins=int, audit_log_udp=int):

        self.name = name
        self.mobile = mobile
        self.office = office
        self.photo = photo
        self.mail = mail
        self.active = active
        self.audit_fec_first_time = self.date_now()
        self.audit_log_ins = audit_log_ins
        self.audit_fec_ins = self.date_now()
        self.audit_log_udp = audit_log_udp
        self.audit_fec_udp = self.date_now()

    def date_now(self):

        x = datetime.datetime.now()
        return x
