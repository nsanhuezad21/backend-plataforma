import datetime

class Password:

    def __init__(self, id_fk_usuario: int, intentos: int, bloqueado: bool, fecha_bloqueo: datetime, audit_log_ins:int):

        self.id_fk_usuario = id_fk_usuario
        self.intentos = intentos
        self.bloqueado = bloqueado
        self.fecha_bloqueo = fecha_bloqueo
        self.audit_log_ins = audit_log_ins
        self.audit_fec_ins = self.date_now()

    def date_now(self):

        x = datetime.datetime.now()
        return x