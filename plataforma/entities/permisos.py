import datetime

class Permiso:

	def __init__(self, idreg_fk_perfil: int ,idreg_fk_menu: int, acceder: bool, listar: bool, add: bool, edit: bool, delete: bool,active: str, audit_log_ins: int, audit_log_udp: int):

		self.idreg_fk_perfil = idreg_fk_perfil
		self.idreg_fk_menu = idreg_fk_menu
		self.acceder = acceder
		self.listar = listar
		self.add = add
		self.edit = edit
		self.delete = delete
		self.active = active
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = self.date_now()
		self.audit_log_udp = audit_log_udp
		self.audit_fec_udp = self.date_now()

	def date_now(self):

		x = datetime.datetime.now()
		return x