import datetime

class Perfil:

	def __init__(self, name_perfil: str,active: str, audit_log_ins: int, audit_log_udp: int):

		self.name_perfil = name_perfil
		self.active = active
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = self.date_now()
		self.audit_log_udp = audit_log_udp
		self.audit_fec_udp = self.date_now()

	def date_now(self):

		x = datetime.datetime.now()
		return x