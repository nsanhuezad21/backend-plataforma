import datetime

class Session:

	def __init__(self, id_fk_usuario: str, session_ip: str, session_host: str, session_navegador: str, session_start_time: datetime, session_end_time: datetime, session_time: int, active: str, audit_fec_ins: datetime):

		self.id_fk_usuario = id_fk_usuario
		self.session_ip = session_ip
		self.session_host = session_host
		self.session_navegador = session_navegador
		self.session_start_time = session_start_time
		self.session_end_time = session_end_time
		self.session_time = session_time
		self.active = active
		self.audit_fec_ins = audit_fec_ins
