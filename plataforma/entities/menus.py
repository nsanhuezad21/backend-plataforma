import datetime

class Menus:

	def __init__(self, name_menu: str, icon: str, raiz: int, link: str, seguridad: str, active: str, audit_log_ins: int, audit_log_udp: int):

		self.name_menu = name_menu
		self.icon = icon
		self.raiz = raiz
		self.link = link
		self.seguridad = seguridad
		self.active = active
		self.audit_log_ins = audit_log_ins
		self.audit_fec_ins = self.date_now()
		self.audit_log_udp = audit_log_udp
		self.audit_fec_udp = self.date_now()

	def date_now(self):

		x = datetime.datetime.now()
		return x