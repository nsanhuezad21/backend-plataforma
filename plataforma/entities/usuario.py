import datetime

from flask_bcrypt import generate_password_hash


class Usuario:
    def __init__(self, idreg_fk_perfil:int, idreg_fk_cliente : int, user_name:str, name:str, mobile:str, home:str, office:str, gender:str, password:str,
                 empresa:str, photo:str, user_mail:str, active:'s', audit_log_ins:int, audit_log_udp:int):

        self.idreg_fk_perfil = idreg_fk_perfil
        self.idreg_fk_cliente = idreg_fk_cliente
        self.user_name = user_name
        self.name = name
        self.mobile = mobile
        self.home = home
        self.office = office
        self.gender = gender
        self.password = generate_password_hash(password).decode('utf-8')
        self.empresa = empresa
        self.photo = photo
        self.user_mail = user_mail
        self.active = active
        self.audit_log_ins = audit_log_ins
        self.audit_fec_ins = self.date_now()
        self.audit_log_udp = audit_log_udp
        self.audit_fec_udp = self.date_now()

    def date_now(self):

        x = datetime.datetime.now()
        return x
