def desencriptar_valor(y):
    y = str(y)
    x1=y.replace('QTfUma', '0')
    x2=x1.replace('bgzGyO', '1')
    x3=x2.replace('FDWBPF', '2')
    x4=x3.replace('eGztUP', '3')
    x5=x4.replace('PDddDE', '4')
    x6=x5.replace('WcKXrz', '5')
    x7=x6.replace('SiKcEr', '6')
    x8=x7.replace('UGKhMh', '7')
    x9=x8.replace('bVtzBe', '8')
    x0=x9.replace('KazLGg', '9')
    return x0

def encriptar_valor(p):
    p = str(p)
    x1=p.replace('0', 'QTfUma')
    x2=x1.replace('1', 'bgzGyO')
    x3=x2.replace('2', 'FDWBPF')
    x4=x3.replace('3', 'eGztUP')
    x5=x4.replace('4', 'PDddDE')
    x6=x5.replace('5', 'WcKXrz')
    x7=x6.replace('6', 'SiKcEr')
    x8=x7.replace('7', 'UGKhMh')
    x9=x8.replace('8', 'bVtzBe')
    x0=x9.replace('9', 'KazLGg')
    return x0