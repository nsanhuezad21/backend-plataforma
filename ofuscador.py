class Ofuscador:

    def __init__(self, place: str, tipo: str, ofuscador: str, limite : int, requerido:str):

        self.place =  place
        self.tipo = tipo
        self.ofuscador = ofuscador
        self.limite = limite
        self.requerido = requerido

# ofuscador login
oUsuario = Ofuscador("usuario", "text", "X8fs5js9qa", 250, "required")
oContraseña = Ofuscador("**********", "password", "X8fs5js8qa", 500, "required")

OfuscadorLogin = [oUsuario, oContraseña]

#ofuscador Usuario

OPerfil = Ofuscador("Seleccione un Perfil", "select", "U8fs5js0qa", 1, "required")
OCliente = Ofuscador("Seleccione una Empresa", "select", "U8fs5js0qb", 1, "required")
OUsuario = Ofuscador("Usuario", "text", "U8fs5js1qa", 250, "required")
ONombre = Ofuscador("Nombre", "text", "U8fs5js2qa", 250, "required")
OTelefono = Ofuscador("telefono", "text", "U8fs5js3qa", 250, " ")
OHome = Ofuscador("Direccion", "text", "U8fs5js4qa", 250, " ")
Oficina = Ofuscador("Oficina", "text", "U8fs5js5qa", 250, " ")
OGenero = Ofuscador("Seleccione un genero", "select", "U8fs5js6qa", 1, "required")
OContraseñaU = Ofuscador("Ingrese Contraseña", "password", "U8fs5js7qa", 500, "required")
OEmpresa = Ofuscador("Empresa", "text", "U8fs5js8qa", 500, " ")
OFoto = Ofuscador("Seleccione una imagen", "image", "U8fs5js9qa", 500, " ")
OCorreo = Ofuscador("correo@dominio.cl", "email", "U8fs5js1qb", 500, "required")

OfuscadorUsuario = [ OPerfil, OCliente, OUsuario, ONombre, OTelefono, OHome, Oficina, OGenero, OContraseñaU,
OEmpresa, OFoto, OCorreo]

#ofuscador permiso

OPerfilP = Ofuscador("Seleccione un Perfil", "select", "T8fs5js0qa", 1, "required")
OMenuP = Ofuscador("Seleccione un Menu", "select", "T8fs5js0qb", 1, "required")
OAcceder = Ofuscador("True", "checkbox", "T8fs5js1q", 1, "required")
OListar = Ofuscador("True", "checkbox", "T8fs5js2q", 1, "required")
OAdd = Ofuscador("True", "checkbox", "T8fs5js3q", 1, "required")
OEdit = Ofuscador("True", "checkbox", "T8fs5js4q", 1, "required")
ODelete = Ofuscador("True", "checkbox", "T8fs5js5q", 1, "required")

OfuscadorPermiso = [ OPerfilP, OMenuP, OAcceder, OListar, OAdd, OEdit, ODelete]

#ofuscador menus
ONameM = Ofuscador("Nombre Menu", "text", "M8fs5js1q", 50, "required")
OIconM = Ofuscador("Icono", "text", "M8fs5js2q", 50, "required")
ORaizM = Ofuscador("Raiz", "number", "M8fs5js3q", 50, "required")
OlinkM = Ofuscador("Link", "text", "M8fs5js4q", 255, "required")
OSeguridadM = Ofuscador("seguridad", "text", "M8fs5js5q", 255, "required")

OfuscadorMenu = [ ONameM, OIconM, ORaizM, OlinkM, OSeguridadM ]

#ofuscador perfil

oNameP = Ofuscador("Nombre PErfil", "text", "P8fs5js1q", 500, "required")

OfuscadorPerfil = [oNameP]