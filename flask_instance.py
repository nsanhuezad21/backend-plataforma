import os
from base64 import b64encode
import random
import string
from functools import update_wrapper

from itsdangerous import encoding
from flask import Flask, url_for, request
from flask._compat import integer_types, reraise, string_types, text_type

from flask.helpers import (
    _PackageBoundObject,
    _endpoint_from_view_func, find_package, get_env, get_debug_flag,
    get_flashed_messages, locked_cached_property, url_for, get_load_dotenv
)


import middleware

#import improve_bot.models as models



def setupmethod(f):
    """Wraps a method so that it performs a check in debug mode if the
    first request was already handled.
    """
    def wrapper_func(self, *args, **kwargs):
        if self.debug and self._got_first_request:
            raise AssertionError('A setup function was called after the '
                'first request was handled.  This usually indicates a bug '
                'in the application where a module was not imported '
                'and decorators or other functionality was called too late.\n'
                'To fix this make sure to import all your view modules, '
                'database models and everything related at a central place '
                'before the application starts serving requests.')
        return f(self, *args, **kwargs)
    return update_wrapper(wrapper_func, f)


class FlaskInstance(Flask):


    @setupmethod
    def add_url_rule(self, rule, endpoint=None, view_func=None,
                     provide_automatic_options=None, **options):
        """Connects a URL rule.  Works exactly like the :meth:`route`
        decorator.  If a view_func is provided it will be registered with the
        endpoint."""

        if endpoint is None:
            endpoint = _endpoint_from_view_func(view_func)
        options['endpoint'] = endpoint
        methods = options.pop('methods', None)

        # if the methods are not given and the view_func object knows its
        # methods we can use that instead.  If neither exists, we go with
        # a tuple of only ``GET`` as default.

        if methods is None:
            methods = getattr(view_func, 'methods', None) or ('GET',)
        if isinstance(methods, string_types):
            raise TypeError('Allowed methods have to be iterables of strings, '
                            'for example: @app.route(..., methods=["POST"])')
        methods = set(item.upper() for item in methods)

        # Methods that should always be added
        required_methods = set(getattr(view_func, 'required_methods', ()))


        if provide_automatic_options is None:
            if 'OPTIONS' not in methods:
                provide_automatic_options = True
                required_methods.add('OPTIONS')
            else:
                provide_automatic_options = False

        # Add the required methods now.
        methods |= required_methods


        #INICIO FRAMEWORK SEGURIDAD

        #selector segun parametro en la base de datos, que da a elegir si quiero o no esto
        
        try:
            fr_query = models.TBL_PARAMETROS.get(models.TBL_PARAMETROS.PARAMETER_NAME =='Framework de seguridad')
        except:

            fr_query = None

        if fr_query != None:

            fr = fr_query.VALUE_PARAMETER

            if fr == 'si' or fr == 'Si':

                fr_selector = True
                #print('fr_selector si')

            else:
                fr_selector = False
                #print('fr_selector no')

        else:

            #fr_selector = False
            fr_selector = True   
        
        if fr_selector == True : 
            if rule != '/' and not 'static' in rule and not '.ico' in rule and rule != '/inicio' and rule != '/favicon.ico' :
                route = rule
                self.wsgi_app = middleware.SimpleMiddleWare(self.wsgi_app)

                #esta es la unica dependencia para generar los links, de lo contrario no podriamos acceder a rutas encriptadas

                link = self.wsgi_app.generate_route(route)

                if '<' in rule:
                #un token random que es irrelevante en la ruta
                    argument = rule.split('<')[1].rstrip('>')

                    arg = encoding.base64_encode(argument)

                    #rule = '/'+link +'/<'+str(arg.decode('utf-8'))+'>/'
                    rule =  '/'+link +'/<'+argument+'>/'
                else:
                    rule = '/'+link
            else:
                pass
        else:
            pass

	#--framework seguridad---


        rule = self.url_rule_class(rule, methods=methods, **options)
        rule.provide_automatic_options = provide_automatic_options

        self.url_map.add(rule)
        if view_func is not None:
            old_func = self.view_functions.get(endpoint)
            if old_func is not None and old_func != view_func:
                raise AssertionError('View function mapping is overwriting an '
                                     'existing endpoint function: %s' % endpoint)
            self.view_functions[endpoint] = view_func





