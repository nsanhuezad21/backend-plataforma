import datetime
import psycopg2

bbdd = "test"
user = "postgres"
password = "postgres"

# servicio que esta borrando las sesiones despues de 10 minutos
def doQuery( conn ) :
    tim = conn.cursor()
    tim.execute("SELECT value_parameter from tbl_parametros where parameter_name = 'tiempo_sesion'")
    limite_sesion = 0
    for tiem in tim.fetchall():
        limite_sesion = int(tiem[0])

    cur = conn.cursor()

    cur.execute( "SELECT id, session_start_time from tbl_session" )

    for sesion in cur.fetchall() :
        tiempo = datetime.datetime.now() - sesion[1] 
        minuto = (tiempo.seconds // 60) % 60
        if minuto > limite_sesion:
            delete = conn.cursor()
            delete.execute("DELETE from tbl_session where id =" +str(sesion[0]))
            conn.commit()
        else:
            pass
        


myConnection = psycopg2.connect( host="localhost", user=user, password=password, dbname=bbdd )
doQuery( myConnection )
myConnection.close()



