import json
import subprocess
from sqlalchemy.schema import (
    MetaData,
    Table,
    DropTable,
    ForeignKeyConstraint,
    DropConstraint,
)
from sqlalchemy.engine import reflection
from sqlalchemy import MetaData

from plataforma.repositories.sql_alchemy.menus_sql_alchemy import MenuRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.parametros_sql_alchemy import ParametrosRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.perfiles_sql_alchemy import PerfilRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.permiso_sql_alchemy import permisoRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.sesiones_sql_alchemy import SesionRepositorySqlAlchemy
from plataforma.repositories.sql_alchemy.usuario_sql_alchemy import UsuarioRepositorySqlAlchemy


from plataforma.repositories.sql_alchemy.mapping.menus_mapping import menu_mapping
from plataforma.repositories.sql_alchemy.mapping.parametros_mapping import parametro_mapping
from plataforma.repositories.sql_alchemy.mapping.perfiles_mapping import perfil_mapping
from plataforma.repositories.sql_alchemy.mapping.permisos_mapping import permiso_mapping
from plataforma.repositories.sql_alchemy.mapping.sesiones_mapping import session_mapping
from plataforma.repositories.sql_alchemy.mapping.usuario_mapping import usuario_mapping

from plataforma.entities.menus import Menus
from plataforma.entities.parametros import Parametros
from plataforma.entities.perfiles import Perfil
from plataforma.entities.permisos import Permiso
from plataforma.entities.sesiones import Session
from plataforma.entities.usuario import Usuario

from plataforma.engine import engine
from flask_script import Manager

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, clear_mappers

from sqlalchemy import Table, MetaData, Column, Integer, String, Sequence, DateTime
import datetime
import requests

import os
import time

from plataforma import get_app
import random
import string

from flask_sqlalchemy import SQLAlchemy
from flask import (url_for, current_app)

app = get_app()

manager = Manager(app)

def db_DropEverything(db):
    # From http://www.sqlalchemy.org/trac/wiki/UsageRecipes/DropEverything

    conn = db.engine.connect()

    # the transaction only applies if the DB supports
    # transactional DDL, i.e. Postgresql, MS SQL Server
    trans = conn.begin()

    inspector = reflection.Inspector.from_engine(db.engine)

    # gather all data first before dropping anything.
    # some DBs lock after things have been dropped in
    # a transaction.
    metadata = MetaData()

    tbs = []
    all_fks = []

    for table_name in inspector.get_table_names():
        fks = []
        for fk in inspector.get_foreign_keys(table_name):
            if not fk['name']:
                continue
            fks.append(
                ForeignKeyConstraint((), (), name=fk['name'])
            )
        t = Table(table_name, metadata, *fks)
        tbs.append(t)
        all_fks.extend(fks)

    for fkc in all_fks:
        conn.execute(DropConstraint(fkc))

    for table in tbs:
        conn.execute(DropTable(table))

    trans.commit()

@manager.command
def all():
    drop_all()
    init()
    base()

@manager.command
def drop_all():

    db_DropEverything(engine)
    db = SQLAlchemy(app)
    db.session.execute("ALTER SEQUENCE menus_id_seq RESTART WITH 1")
    db.session.commit()
    db.session.execute("ALTER SEQUENCE parametros_id_seq RESTART WITH 1")
    db.session.commit()
    db.session.execute("ALTER SEQUENCE perfil_id_seq RESTART WITH 1")
    db.session.commit()
    db.session.execute("ALTER SEQUENCE permiso_id_seq RESTART WITH 1")
    db.session.commit()
    db.session.execute("ALTER SEQUENCE sesiones_id_seq RESTART WITH 1")
    db.session.commit()
    db.session.execute("ALTER SEQUENCE usuario_id_seq RESTART WITH 1")
    db.session.commit()

    print('OK drop')

@manager.command
def init():

    metadata = MetaData()
    clear_mappers()

    perfil_mapping(metadata)
    menu_mapping(metadata)
    parametro_mapping(metadata)
    permiso_mapping(metadata)
    usuario_mapping(metadata)
    session_mapping(metadata)

    metadata.create_all(engine)

    print('OK init')

@manager.command
def base():
    db = SQLAlchemy(app)
    
    perfil_repo = PerfilRepositorySqlAlchemy(db)

    dev = Perfil('Desarrollador', 's', 1, 1)
    admin = Perfil('Administrador', 's', 1, 1)

    perfil_repo.add(admin)
    perfil_repo.add(dev)
    
    user_repo = UsuarioRepositorySqlAlchemy(db)

    user_repo.add(Usuario(dev.id, "nsanhueza", "natanael", "935124007", "casa", "michimalonco 1002", "m", "200211790", "adelaida", "none_imagen", "nsanhuezad21@gmail.com", "s", datetime.datetime.now(), 1, 1))

    db.session.commit()